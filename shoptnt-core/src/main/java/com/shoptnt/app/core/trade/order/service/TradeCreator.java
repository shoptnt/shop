/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.trade.order.service;

import com.shoptnt.app.core.trade.order.model.vo.TradeVO;

/**
 * Created by kingapex on 2019-01-23.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-23
 */
public interface TradeCreator {

    /**
     * 检测配送范围
     *
     * @return
     */
    TradeCreator checkShipRange();

    /**
     * 检测商品合法性
     *
     * @return
     */
    TradeCreator checkGoods();

    /**
     * 检测促销活动合法性
     *
     * @return
     */
    TradeCreator checkPromotion();

    /**
     * 创建交易
     *
     * @return
     */
    TradeVO createTrade();

}
