/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by kingapex on 2018/3/21.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/21
 */
@SpringBootApplication
@ComponentScan("com.shoptnt.app")
public class ShoptntApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShoptntApplication.class, args);
    }

}
