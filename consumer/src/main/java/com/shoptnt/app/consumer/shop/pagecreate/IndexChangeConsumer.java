/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.consumer.shop.pagecreate;


import com.shoptnt.app.consumer.core.event.CategoryChangeEvent;
import com.shoptnt.app.consumer.core.event.IndexChangeEvent;
import com.shoptnt.app.consumer.core.event.MobileIndexChangeEvent;
import com.shoptnt.app.consumer.shop.pagecreate.service.PageCreator;
import com.shoptnt.app.core.base.message.CategoryChangeMsg;
import com.shoptnt.app.core.base.message.CmsManageMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 首页生成
 *
 * @author zh
 * @version v1.0
 * @since v6.4.0
 * 2017年8月29日 下午3:41:18
 */
@Component
public class IndexChangeConsumer implements IndexChangeEvent, CategoryChangeEvent, MobileIndexChangeEvent {

    @Autowired
    private PageCreator pageCreator;

    /**
     * 生成首页
     */
    @Override
    public void createIndexPage(CmsManageMsg operation) {
        this.createIndex();
    }

    @Override
    public void categoryChange(CategoryChangeMsg categoryChangeMsg) {
        this.createIndex();
    }

    private void createIndex() {
        try {
            // 生成静态页面
            pageCreator.createIndex();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mobileIndexChange(CmsManageMsg operation) {
        this.createIndex();
    }

}
