/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.consumer.shop.message;

import com.shoptnt.app.consumer.core.event.SmsSendMessageEvent;
import com.shoptnt.app.core.base.model.vo.SmsSendVO;
import com.shoptnt.app.core.client.system.SmsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 发送短信
 *
 * @author zjp
 * @version v7.0
 * @since v7.0
 * 2018年3月25日 下午3:15:01
 */
@Component
public class SmsMessageConsumer implements SmsSendMessageEvent {

    @Autowired
    private SmsClient smsClient;

    @Override
    public void send(SmsSendVO smsSendVO) {
        smsClient.send(smsSendVO);
    }
}
