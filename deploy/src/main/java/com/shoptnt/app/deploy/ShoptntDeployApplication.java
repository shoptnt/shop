/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.deploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;


/**
 * shoptnt 安装器工程
 *
 * @author kingapex
 * @version v1.0.0
 * @since v7.0.0
 * 2018年1月22日 上午10:01:32
 */
@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan(value = "com.shoptnt.app.framework.database," +
        "com.shoptnt.app.framework.context," +
        "com.shoptnt.app.framework.exception," +
        "com.shoptnt.app.framework.redis.configure.builders,"+
        "com.shoptnt.app.deploy")
public class ShoptntDeployApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ShoptntDeployApplication.class, args);

    }
}
