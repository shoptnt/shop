# ShopTNT B2C商城系统


## 官网： http://www.shoptnt.cn

ShopTNT是Java开发的一款B2C商城系统，代码全开源，前端使用Vue+uniapp开发，前后端分离
商城包含：PC、小程序、H5、App

## 多店铺商城系统已开源，需要学习的同学可以通过最下方项目开源地址自行下载。

## 文档： http://docs.shoptnt.cn

## 使用须知

1. 允许个人学习使用
2. 允许用于学习、毕业设计等
3. 限制商业使用，如需[商业使用](http://www.shoptnt.cn)联系QQ：2025555598 

## 交流、反馈
官方QQ群：
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=RtRIL7WomrO79uDDDp4HihX_GH1xMIPD&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="ShopTNT开源交流群" title="ShopTNT开源交流群"></a> 766503360

## 主要功能
商品管理、订单管理、会员管理、促销管理、页面设置（楼层装修）、统计分析、消息管理、分销管理、系统设置、开发配置
用户端、第三方登录、第三方支付

系统包含搜索引擎、多级缓存、分布式事务、分布式任务调度等，支持Docker、K8S、Jenkins部署

项目地址：

Java后端：https://gitee.com/shoptnt/shop.git

PC端：https://gitee.com/shoptnt/shoptnt-ui.git

配置中心：https://gitee.com/shoptnt/shoptnt-config.git

## B2B2C项目开源地址

后台API：https://gitee.com/bbc-se/api

前端ui：https://gitee.com/bbc-se/pc-ui

移动端：https://gitee.com/bbc-se/mobile-ui

配置中心：https://gitee.com/bbc-se/config

