/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.framework.security.model;
/**
 * 定义Token基本常量
 * @author zh
 * @version v7.0
 * @date 18/4/12 下午2:46
 * @since v7.0
 */
public class TokenConstant {
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";

}
